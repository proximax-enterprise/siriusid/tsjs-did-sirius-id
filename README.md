# tsjs-did-siriusid

This is the ProximaX SiriusId DID Method library based on the [specification](docs/did-method-spec.md), which allows you to do the following

- Registering, updating and resolving the identifier.

## Requirements

- NodeJS 10.x
- Typescript >= 3.5.3

## Installing the library

Install using [npm](https://www.npmjs.org/):

```shell
npm install tsjs-did-siriusid
```

or using [yarn](https://yarnpkg.com/):

```shell
yarn add tsjs-did-siriusid
```

## Getting started

Initiates the registry provider

```ts
const registryProvider = new SiriusChainProvider(
  'http://bctestnet1.brimstone.xpxsirius.io:3000'
);
```

Initiates the storage provider

```ts
const storageProvider = new IpfsStorageProvider(
  'http://ipfs1-dev.xpxsirius.io:5001'
);
```

Initiates an instance of the SiriusId

```ts
const siriusId = await SiriusId.createInstance(registryProvider, storageProvider);
```

Creates an identity

```ts
const identity = await siriusId.create();

// Keep this master private key for future operations
const masterPrivateKey = identity.account.privateKey;

// did method of Sirius id
console.log(identity.did);

// did document in JSON_LD
console.log(identity.didDocument);
// the qr code represents Sirius id
console.log(identity.qrCode);
// the master account registered in Sirius Chain
console.log(identity.account);
```

Resolves the identity

```ts
const did = 'did:sirius:2U6DTLvttXXsJjcDhLvGXKAgsUzf7HUT9kUns1iqCc';
const didDocument = await siriusId.resolve(did);
```

Updates the identity document with the public profile

```ts
const publicProfile = {
name: 'Thomas Tran 3',
email: 'dev.gp@gmail.com',
country: 'AU',
phoneNumber: null,
};

// store in ipfs
const profileContent = JSON.stringify(publicProfile);

const docStream = StreamHelper.string2Stream(profileContent);

const publicProfileResource = await storageProvider.save(docStream);


// generate public profile service section
// and update the did document
const identity = await siriusId.update(account.privateKey, document => {
  // clear public profile otherwise it will use the existing one
  document.services = document.services.find(s => s.type !== SIRIUS_ID_PUBLIC_PROFILE_ENTITY);
  
  document.addServiceSection(
      ServiceSection.createPublicProfileService(
        document.did,
        publicProfileResource,
      ),
  );
});
```

## Contributing

### Bootstrap your environments

1. Clone this repository.

```sh
git clone https://gitlab.com/thomas.tran/tsjs-did-sirius-id
```

2. Install the dependency packages.

```sh
npm install
```

### Running tests

```sh
npm run test
```

### Build the packages

```sh
npm run build
```

### Publish the packages

 ```sh
npm publish --registry http://localhost:4873/
 ```

Install [Verdaccio](https://verdaccio.org/docs/en/installation) as local registry

## FAQs

1. I encountered the following issue when using the library with web frameworks

```js
Cannot resolve module 'net' , 'tls', 'fs', 'buffer', 'crypto'
```

- Create patch.js file in the root folder
- Add the following content to the patch.js file

```js
const fs = require('fs');

function replaceFile(filePath, searchString, replaceString) {
    fs.readFile(filePath, 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        const result = data.replace(searchString, replaceString);

        fs.writeFile(filePath, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
    });
}

const f = 'node_modules/@angular-devkit/build-angular/src/angular-cli-files/models/webpack-configs/browser.js';
replaceFile(f, /node: false/g, "node: {fs: 'empty', global: true, crypto: 'empty', tls: 'empty', net: 'empty', process: true, module: false, clearImmediate: false, setImmediate: false}");

f_crypto = 'node_modules/tsjs-xpx-chain-sdk/dist/src/core/crypto/Crypto.js';
replaceFile(f_crypto, 'require(\'crypto\');', 'require(\'crypto-browserify\');');
```

- Add the following content to pollfills.js

```js
(window as any).global = window;
global.Buffer = global.Buffer || require('buffer').Buffer;
(window as any).process = {
  env: { DEBUG: undefined },
};
```

- Run the command

```js
node patch.js
```
