import test from 'ava';
import { KeyVaultProvider } from './key-vault-provider';

test('should generate keypair for valid network identifier', t => {
  const keyVault = new KeyVaultProvider();
  const networkId = '0xa8';
  const keyPair = keyVault.generateKeyPair(networkId);
  t.log(keyPair);
  t.true(keyPair.privateKey.length === 64);
  t.true(keyPair.publicKey.length === 64);
});
