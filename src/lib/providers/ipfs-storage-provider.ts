import { Stream } from 'stream';
import { ResourceHash } from '../common/types';
import { IStorageProvider } from '../common/providers';
import { STORAGE_PROVIDER_UNAVAILABLE } from '../common/constants';

/**
 * @class
 * Sirius ID specific storage provider using IPFS protocol
 */
export class IpfsStorageProvider implements IStorageProvider {
  private type: string = 'IPFS Storage';
  private ipfs: any;

  /**
   * Creates an instance of {@link IpfsStorageProvider}
   * @param provider provider the storage provider
   */
  constructor(provider: string) {
    if (!provider) {
      throw new Error(STORAGE_PROVIDER_UNAVAILABLE);
    }

    const ipfsClient = require('ipfs-http-client');

    this.ipfs = new ipfsClient(provider);
  }

  /**
   * Returns the sorage provider type
   * @returns { type }  the storage provider type
   */
  public getType(): string {
    return this.type;
  }

  /**
   * Saves the stream on IPFS
   * @param stream the stream to store
   * @returns { hash, url } the stream content hash and the stream content url
   */
  public async save(stream: Stream): Promise<ResourceHash> {
    if (!stream) {
      throw new Error('stream is required');
    }

    const result = await this.ipfs.add(stream, { pin: true });

    const hash = result[0].hash;

    const { host, port, protocol } = this.getIpfsEndPointConfig();

    const url = `${protocol}://${host}:${port}/ipfs/${hash}`;

    return { hash, url };
  }

  /**
   * Remove the content from storage
   * @param dataHash the datahash to be unpin
   */
  public async remove(dataHash: string): Promise<any> {
    if (!dataHash) {
      throw new Error('dataHash is required');
    }

    const result = await this.ipfs.pin.rm(dataHash);

    // call garbage collection
    await this.ipfs.repo.gc();

    return result;
  }

  /**
   * Reads the stream content given a datahash
   * @param dataHash the datahash of the stream content
   * @returns { stream } the stream content
   */
  public async read(dataHash: string): Promise<Stream> {
    if (!dataHash) {
      throw new Error('dataHash is required');
    }

    const result = await this.ipfs.catReadableStream(dataHash);

    return result;
  }

  public async isOnline(): Promise<boolean> {
    try {
      return this.ipfs.repo.version() !== null;
    } catch (err) {
      return Promise.resolve(false);
    }
  }

  /**
   * Get the ipfs provider endpoint config
   */
  private getIpfsEndPointConfig() {
    return this.ipfs.getEndpointConfig();
  }
}
