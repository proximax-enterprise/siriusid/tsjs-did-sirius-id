import test from 'ava';
import { IpfsStorageProvider } from './ipfs-storage-provider';
import { STORAGE_PROVIDER_UNAVAILABLE } from '../common/constants';

import { StreamHelper } from '../utils/stream-helper';

const STORAGE_URL = 'http://ipfs1-dev.xpxsirius.io:5001';
test('should return status online', async t => {
  const ipfsStorage = new IpfsStorageProvider(STORAGE_URL);
  const isOnline = await ipfsStorage.isOnline();

  t.log(isOnline);
  t.true(isOnline);
});

test('should throw error if storage provider is not available', async t => {
  const error = t.throws(() => new IpfsStorageProvider(''));
  t.log(error);
  t.is(error.message, STORAGE_PROVIDER_UNAVAILABLE);
}, 3000);

test('should add content to storage', async t => {
  const ipfsStorage = new IpfsStorageProvider(STORAGE_URL);
  const stream = StreamHelper.string2Stream('test string');
  const result = await ipfsStorage.save(stream);

  t.log(result);
  t.is(result.hash, 'Qmbn6ZFGa6m5gtz911Lwe4oBNzfnK61SeDEKeUzCbXUVmV');

  const content = await ipfsStorage.read(result.hash);
  const docContent = await StreamHelper.stream2String(content);
  t.log(docContent);
}, 3000);

test('should remove content from storage', async t => {
  const ipfsStorage = new IpfsStorageProvider(STORAGE_URL);
  const stream = StreamHelper.string2Stream('test string 2');
  const result = await ipfsStorage.save(stream);

  // remove from network
  const removeResult = await ipfsStorage.remove(result.hash);
  t.log(removeResult);

  // verify the content is still in the network
  const content = await ipfsStorage.read(result.hash);
  const docContent = await StreamHelper.stream2String(content);
  t.log(docContent);
}, 3000);
