import {
  NetworkHttp,
  NetworkType,
  Account,
  MetadataHttp,
  BlockHttp,
  Deadline,
  ModifyAccountMetadataTransactionBuilder,
  MetadataModification,
  MetadataModificationType,
  FeeCalculationStrategy,
  TransactionHttp,
} from 'tsjs-xpx-chain-sdk';
import { ResourceHash, IKeyPair, NetworkIdentifer } from '../common/types';
import { IRegistryProvider } from '../common/providers';
import { toNetworkIdentifier } from '../utils/converter';
import { REGISTRY_PROVIDER_UNAVAILABLE } from '../common/constants';
import { SiriusAccount } from '../sirius-account';

export class SiriusChainProvider implements IRegistryProvider {
  private networkHttp: NetworkHttp;
  private metadataHttp: MetadataHttp;
  private blockHttp: BlockHttp;
  private transactionHttp: TransactionHttp;

  constructor(provider: string) {
    if (!provider) {
      throw new Error(REGISTRY_PROVIDER_UNAVAILABLE);
    }

    this.networkHttp = new NetworkHttp(provider);
    this.metadataHttp = new MetadataHttp(provider);
    this.blockHttp = new BlockHttp(provider);
    this.transactionHttp = new TransactionHttp(provider);
  }

  public async createAccountFromPrivateKey(
    privateKey: string,
  ): Promise<Account> {
    const networkType = await this.getNetworkType();
    const account = Account.createFromPrivateKey(privateKey, networkType);
    return account;
  }

  public async createNewAccount(): Promise<Account> {
    const networkType = await this.getNetworkType();
    const account = Account.generateNewAccount(networkType);
    return account;
  }

  public async getNetworkType(): Promise<NetworkType> {
    return this.networkHttp.getNetworkType().toPromise();
  }

  public async getNetwork(): Promise<NetworkIdentifer> {
    const networkType = await this.networkHttp.getNetworkType().toPromise();
    return toNetworkIdentifier(networkType);
  }

  public async createAccount(): Promise<SiriusAccount> {
    const networkType = await this.getNetworkType();
    const account = Account.generateNewAccount(networkType);
    return account;
  }

  public async createAccountFromKey(
    privateKey: string,
  ): Promise<SiriusAccount> {
    const networkType = await this.getNetworkType();
    const account = Account.createFromPrivateKey(privateKey, networkType);
    return account;
  }

  public async publish(
    keypair: IKeyPair,
    resource: ResourceHash,
  ): Promise<string> {
    const generationHash = await this.getGenerationHash();
    const networkType = await this.getNetworkType();
    const builder = new ModifyAccountMetadataTransactionBuilder();
    const account = Account.createFromPrivateKey(
      keypair.privateKey,
      networkType,
    );
    const metadataTx = builder
      .address(account.address)
      .deadline(Deadline.create())
      .generationHash(generationHash)
      .modifications([
        new MetadataModification(
          MetadataModificationType.ADD,
          'didHash',
          resource.hash,
        ),
        new MetadataModification(
          MetadataModificationType.ADD,
          'didUrl',
          resource.url,
        ),
      ])
      .networkType(account.address.networkType)
      .feeCalculationStrategy(FeeCalculationStrategy.ZeroFeeCalculationStrategy)
      .build();

    const signedTx = metadataTx.signWith(account, generationHash);

    await this.transactionHttp.announce(signedTx).toPromise();

    return signedTx.hash;
  }

  public async read(address: string): Promise<ResourceHash> {
    const metadata = await this.metadataHttp
      .getAccountMetadata(address)
      .toPromise();

    const hash = metadata.fields.filter(x => x.key === 'didHash')[0].value;
    const url = metadata.fields.filter(x => x.key === 'didUrl')[0].value;

    return { hash, url };
  }

  public async isOnline(): Promise<boolean> {
    let isOnline = false;
    try {
      const nodeInfo = await this.networkHttp.getNetworkType().toPromise();

      if (nodeInfo) {
        isOnline = true;
      }
    } catch (error) {
      isOnline = false;
    }
    return Promise.resolve(isOnline);
  }

  private async getGenerationHash(): Promise<string> {
    return (await this.blockHttp.getBlockByHeight(1).toPromise())
      .generationHash;
  }
}
