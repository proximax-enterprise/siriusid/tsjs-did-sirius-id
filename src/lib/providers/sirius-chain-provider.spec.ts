import test from 'ava';
import { SiriusChainProvider } from './sirius-chain-provider';
import { REGISTRY_PROVIDER_UNAVAILABLE } from '../common/constants';

test('should throw error if registry provider is not available', async t => {
  const error = t.throws(() => new SiriusChainProvider(''));
  t.log(error);
  t.is(error.message, REGISTRY_PROVIDER_UNAVAILABLE);
});
