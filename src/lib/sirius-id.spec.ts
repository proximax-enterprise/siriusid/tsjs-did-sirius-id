import test from 'ava';

import { SiriusChainProvider } from './providers/sirius-chain-provider';
import { IpfsStorageProvider } from './providers/ipfs-storage-provider';
import { KeyVaultProvider } from './providers/key-vault-provider';
import { ServiceSection } from './dids/document/service';
// import { SiriusPublicProfile } from './common/types';
import { SiriusId } from './sirius-id';
import { StreamHelper } from './utils/stream-helper';
// import { SIRIUS_ID_PUBLIC_PROFILE_ENTITY } from './common/constants';

const REGISTRY_URL = 'https://demo-sc-api-1.ssi.xpxsirius.io';
/// const STORAGE_URL = 'https://ipfs1-dev.xpxsirius.io:5443';
const STORAGE_URL = 'http://ipfs1-dev.xpxsirius.io:5001';

/*
const PRIVATE_KEY =
  '669424B3F1A3C441F9157CEC48DDFFEAE8715EFBD6D0FC0FC70B74DC6ADDB383';*/

const registryProvider = new SiriusChainProvider(REGISTRY_URL);
const storageProvider = new IpfsStorageProvider(STORAGE_URL);

test('should create siriusid instance', async t => {
  const siriusId = await SiriusId.createInstance(
    registryProvider,
    storageProvider,
  );

  t.deepEqual(typeof siriusId.create, 'function');
  t.deepEqual(typeof siriusId.resolve, 'function');
  t.deepEqual(typeof siriusId.update, 'function');
});

test('should create sirius id did from private key', async t => {
  const siriusId = await SiriusId.createInstance(
    registryProvider,
    storageProvider,
  );
  const keyVault = new KeyVaultProvider();
  const { privateKey } = keyVault.generateKeyPair('0xa8');
  const didDocument = await siriusId.create(privateKey);
  // t.log(didDocument);
  t.true(didDocument !== null);
});

test('should create sirius id and return master key and did information', async t => {
  const siriusId = await SiriusId.createInstance(
    registryProvider,
    storageProvider,
  );
  const identity = await siriusId.create();
  /*t.log(identity.account.privateKey);
  t.log(identity.did);
  t.log(identity.didDocument);
  t.log(identity.qrCode);*/
  t.true(identity.account.privateKey.length === 64);
  t.true(identity.account.publicKey.length === 64);
  t.true(identity.did !== null);
  t.true(identity.didDocument !== null);
}, 5000);

test('should create sirius id with public profile and return master key and did information', async t => {

  const siriusId = await SiriusId.createInstance(
    registryProvider,
    storageProvider,
  );

  const publicProfile = {
    organizationName: 'Proximax',
    establishment: 2018,
  };

  // store in ipfs
  const profileContent = JSON.stringify(publicProfile);

  const docStream = StreamHelper.string2Stream(profileContent);

  // const ipfsProvider = new IpfsStorageProvider(STORAGE_URL);
  const publicProfileResource = await storageProvider.save(docStream);
  // t.log(publicProfileResource);
  // t.log(siriusId);

  const identity = await siriusId.create(undefined, document => {
    document.addServiceSection(
      ServiceSection.createPublicProfileService(
        document.did,
        publicProfileResource,
      ),
    );
  });

  // t.log(identity.account.privateKey);
  // t.log(identity.did);
  // t.log(identity.didDocument);
  // t.log(identity.qrCode);
  t.true(identity.account.privateKey.length === 64);
  t.true(identity.account.publicKey.length === 64);
  t.true(identity.did !== null);
  t.true(identity.didDocument !== null);
}, 5000);

test('should resolve sirius id did', async t => {
  // const did = 'did:sirius:2U6D6vYj5s4Ay8V2t785q6GFJvbhB3VMU49kprGwWW';

  const siriusId = await SiriusId.createInstance(
    registryProvider,
    storageProvider,
  );

  // const identity = await siriusId.create(undefined);
  // t.log(identity);
  const didDocument = await siriusId.resolve('did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK');

  // t.log(didDocument);
  t.true(didDocument !== null);
}, 5000);

/*
test('should update sirius id did', async t => {
  const siriusId = await SiriusId.createInstance(
    registryProvider,
    storageProvider,
  );

  const publicProfile = {
    name: 'Thomas Tran 3',
    email: 'dev.gp@gmail.com',
    country: 'AU',
    phoneNumber: null,
  };

  // store in ipfs
  const profileContent = JSON.stringify(publicProfile);

  const docStream = StreamHelper.string2Stream(profileContent);

  const publicProfileResource = await storageProvider.save(docStream);

  const identity = await siriusId.update(PRIVATE_KEY, document => {
    // clear public profile
    document.services = document.services.find(
      s => s.type !== SIRIUS_ID_PUBLIC_PROFILE_ENTITY,
    );
    document.addServiceSection(
      ServiceSection.createPublicProfileService(
        document.did,
        publicProfileResource,
      ),
    );
  });

  t.log(identity);
  t.true(identity !== null);
}, 5000);
*/
