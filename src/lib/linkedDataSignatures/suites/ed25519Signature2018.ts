import {
  ILinkedDataSignature,
  IDigestable,
  ILinkedDataSignatureAttrs,
} from '../../dids/document/document-common';
import {
  Expose,
  Type,
  Transform,
  classToPlain,
  plainToClass,
  Exclude,
} from 'class-transformer';
import { Helpers } from '../../utils/helper';
import { Security } from '../../utils/security';
import { SECURITY_CONTEXT_V1_URL } from '../../common/contexts';
import { normalize } from 'jsonld';
import 'reflect-metadata';

@Exclude()
export class Ed25519Signature2018 implements ILinkedDataSignature, IDigestable {
  @Expose()
  @Type(() => Date)
  @Transform((value: Date) => value && value.toISOString(), {
    toPlainOnly: true,
  })
  get created() {
    return this._created;
  }

  set created(created: Date) {
    this._created = created;
  }

  @Expose()
  get type() {
    return this._type;
  }

  set type(type: string) {
    this._type = type;
  }

  @Expose()
  get nonce() {
    return this._nonce;
  }

  set nonce(nonce: string) {
    this._nonce = nonce;
  }

  @Expose({ name: 'signatureValue' })
  get signature() {
    return this._signatureValue;
  }

  set signature(signature: string) {
    this._signatureValue = signature;
  }

  @Expose()
  get creator(): string {
    return this._creator;
  }

  set creator(creator: string) {
    this._creator = creator;
  }

  get signer() {
    return {
      did: Helpers.getDidFromKeyId(this.creator),
      keyId: this.creator,
    };
  }

  private _type = 'Ed25519Signature2018';
  private _creator: string = '';
  private _created: Date = new Date();
  private _nonce: string = '';
  private _signatureValue: string = '';

  public async digest(): Promise<Buffer> {
    const normalized = await this.normalize();
    return Security.sha256Hash(Buffer.from(normalized));
  }

  public fromJSON(json: ILinkedDataSignatureAttrs): Ed25519Signature2018 {
    return plainToClass(Ed25519Signature2018, json);
  }

  public toJSON(): ILinkedDataSignatureAttrs {
    return classToPlain(this) as ILinkedDataSignatureAttrs;
  }

  private async normalize(): Promise<string> {
    const json: ILinkedDataSignatureAttrs = this.toJSON();

    json['@context'] = SECURITY_CONTEXT_V1_URL;

    delete json.signatureValue;
    delete json.type;
    delete json.id;

    return normalize(json);
  }
}
