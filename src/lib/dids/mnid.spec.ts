import { toMNID, fromMNID } from './mnid';
import test from 'ava';

const NETWORK = '0xa8';
const ADDRESS = 'VD6GFBFZKPVWXRITLCFJ6MLJUMF2X3ZN6U7PQO4U';
const MNID = '2U6FCfwhbyZaWZLDTbmtWvuZn51GT1W8DASmyrzFCK'; // '2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj';
const INVALID_MNID = '2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbc';

test('encode network identifier and address to MNID', t => {
  const encoded = toMNID({ network: NETWORK, address: ADDRESS });

  t.deepEqual(encoded, MNID);
});

test('decode MNID to network identifier and address', t => {
  const { network, address } = fromMNID(MNID);
  t.log(network);
  t.log(address);
  t.deepEqual(network, NETWORK);
  t.deepEqual(address, ADDRESS);
});

test('generateChecksum throw error', t => {
  t.throws(() => fromMNID(INVALID_MNID));
});
