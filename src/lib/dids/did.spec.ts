import test from 'ava';
import { createDid, isDidUrl, parseDidUrl, resolveDid } from './did';
import { INVALID_DID_URL } from '../common/constants';

const NETWORK = '0xa8';
const ADDRESS = 'VA35N5IM24CHDDRCNNHQW2D7MML2NX2MNX7L4BYU';
const MNID = '2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj';
const DID = 'did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj';
const DID_METHOD = 'sirius';

test('should create Did from method specific id', t => {
  const did = createDid({ network: NETWORK, address: ADDRESS });

  t.deepEqual(did, DID);
});

test('should be the did Url', t => {
  const didUrl = isDidUrl(DID);

  t.true(didUrl);
});

test('should retrieve did information from Did Url', t => {
  const { did, method, idstring, fragment } = parseDidUrl(DID);

  t.deepEqual(did, DID);
  t.deepEqual(method, DID_METHOD);
  t.deepEqual(idstring, MNID);
  t.deepEqual(fragment, null);
});

test('should resolve network and address information from Did', t => {
  const { network, address } = resolveDid(DID);

  t.deepEqual(network, NETWORK);
  t.deepEqual(address, ADDRESS);
});

test('should throw error if the did method is invalid', t => {
  const method = 'pxp';
  const invalidDid = `did:${method}:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj`;
  const error = t.throws(() => resolveDid(invalidDid));
  t.log(error);
  t.is(error.message, `Unsupport method '${method}'`);
});

test('should throw error when parsing invalid did', t => {
  const method = 'pxp';
  const invalidDid = `did:${method}`;
  const error = t.throws(() => parseDidUrl(invalidDid));
  t.log(error);
  t.is(error.message, INVALID_DID_URL);
});
