import { Expose, classToPlain, plainToClass, Exclude } from 'class-transformer';
import { IServiceSection } from './document-common';
import { Did, SiriusPublicProfile } from '../../..';
import {
  SIRIUS_ID_PUBLIC_PROFILE_ENTITY,
  SIRIUS_ID_PUBLIC_PROFILE_ENTITY_DESC,
} from '../../common/constants';
import 'reflect-metadata';
@Exclude()
export class ServiceSection {
  /**
   * Get the the service endpoint identifier
   */

  @Expose()
  get id(): string {
    return this._id;
  }

  /**
   * Set the the service endpoint identifier
   */

  set id(id: string) {
    this._id = id;
  }

  /**
   * Get the the service endpoint type
   */

  @Expose()
  get type(): string {
    return this._type;
  }

  /**
   * Set the the service endpoint type
   */

  set type(type: string) {
    this._type = type;
  }

  /**
   * Get the the service endpoint
   */

  @Expose()
  get serviceEndpoint() {
    return this._serviceEndpoint;
  }

  /**
   * Set the the service endpoint
   */

  set serviceEndpoint(service: string) {
    this._serviceEndpoint = service;
  }

  /**
   * Get the the service endpoint description
   */

  @Expose()
  get description() {
    return this._description;
  }

  /**
   * Set the the service endpoint description
   */

  set description(description: string) {
    this._description = description;
  }

  /**
   * Creates an public profile service section
   * @param did the identifier
   * @param publicProfile the public profile service information
   */
  public static createPublicProfileService(
    did: Did,
    publicProfile: SiriusPublicProfile,
  ): ServiceSection {
    const profile = new ServiceSection();
    profile.id = `${did};${SIRIUS_ID_PUBLIC_PROFILE_ENTITY}`;
    profile.serviceEndpoint = publicProfile.url;
    profile.description = SIRIUS_ID_PUBLIC_PROFILE_ENTITY_DESC;
    profile.type = SIRIUS_ID_PUBLIC_PROFILE_ENTITY;
    return profile;
  }

  private _id: string;
  private _type: string;
  private _serviceEndpoint: string;
  private _description: string;

  /**
   * Creates an service section from json
   * @param json the service section in json format
   */
  public fromJSON(json: IServiceSection): ServiceSection {
    return plainToClass(ServiceSection, json);
  }

  /**
   * Serializes service section to JSON
   */
  public toJSON(): IServiceSection {
    return classToPlain(this) as IServiceSection;
  }
}
