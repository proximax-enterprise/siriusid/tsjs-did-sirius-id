import { AuthenticationSection } from './authentication';
import { PublicKeySection } from './public-key';
import { JsonLdContext, Did } from '../../common/types';
import {
  Expose,
  Type,
  Transform,
  classToPlain,
  plainToClass,
  Exclude,
} from 'class-transformer';
import { ServiceSection } from './service';
import { Ed25519Signature2018 } from '../../linkedDataSignatures/suites/ed25519Signature2018';
import {
  IDidDocument,
  ILinkedDataSignature,
  IDigestable,
  ISigner,
} from './document-common';
import { JsonLdHelpers } from '../../utils/jsonld-helper';
import { Helpers } from '../../utils/helper';

import 'reflect-metadata';
import {
  SECURITY_CONTEXT_V1_URL,
  DEFAULT_CONTEXT_URL,
} from '../../common/contexts';
@Exclude()
export class DidDocument implements IDigestable {
  @Expose({ name: '@context' })
  get context(): JsonLdContext {
    return this['_@context'];
  }

  set context(context: JsonLdContext) {
    this['_@context'] = context;
  }

  @Expose({ name: 'id' })
  get did(): string {
    return this._id;
  }

  set did(did: string) {
    this._id = did;
  }

  @Expose()
  @Type(() => AuthenticationSection)
  get authentications(): AuthenticationSection[] {
    return this._authentications;
  }

  set authentications(authentication: AuthenticationSection[]) {
    this._authentications = authentication;
  }

  @Expose()
  @Type(() => PublicKeySection)
  get publicKeys(): PublicKeySection[] {
    return this._publicKeys;
  }

  set publicKeys(value: PublicKeySection[]) {
    this._publicKeys = value;
  }

  @Expose()
  @Type(() => ServiceSection)
  get services(): ServiceSection[] {
    return this._services;
  }

  set services(service: ServiceSection[]) {
    this._services = service;
  }

  @Expose()
  @Transform((value: Date) => value && value.toISOString(), {
    toPlainOnly: true,
  })
  @Transform((value: string) => value && new Date(value), { toClassOnly: true })
  get created(): Date {
    return this._created;
  }

  set created(value: Date) {
    this._created = value;
  }

  @Expose()
  @Transform((value: Date) => value && value.toISOString(), {
    toPlainOnly: true,
  })
  @Transform((value: string) => value && new Date(value), { toClassOnly: true })
  get updated(): Date {
    return this._updated;
  }

  set updated(value: Date) {
    this._updated = value;
  }

  get signer(): ISigner {
    return {
      did: this._id,
      keyId: this._proof.creator,
    };
  }

  get signature(): string {
    return this._proof.signature;
  }

  set signature(signature: string) {
    this._proof.signature = signature;
  }

  @Expose()
  @Type(() => Ed25519Signature2018)
  @Transform(value => value || new Ed25519Signature2018(), {
    toClassOnly: true,
  })
  get proof(): ILinkedDataSignature {
    return this._proof;
  }

  set proof(proof: ILinkedDataSignature) {
    this._proof = proof;
  }

  public static createFromDid(did: Did): DidDocument {
    const didDocument = new DidDocument();
    didDocument.did = did;
    return didDocument;
  }

  public static fromJSON(json: IDidDocument): DidDocument {
    return plainToClass(DidDocument, json);
  }

  private _id: string;
  private _authentications: AuthenticationSection[] = [];
  private _publicKeys: PublicKeySection[] = [];
  private _services: ServiceSection[] = [];
  private _created: Date = new Date();
  private _updated: Date = new Date();
  private '_@context': JsonLdContext = [
    DEFAULT_CONTEXT_URL,
    SECURITY_CONTEXT_V1_URL,
  ];
  private _proof: ILinkedDataSignature;

  public addAuthenticationSection(section: AuthenticationSection) {
    const hasSection = this.authentications.find(
      s => s.publicKey === section.publicKey,
    );
    if (!hasSection) {
      this.authentications.push(section);
    }
  }

  public addPublicKeySection(section: PublicKeySection) {
    const hasSection = this.publicKeys.find(s => s.id === section.id);
    if (!hasSection) {
      this.publicKeys.push(section);
    }
  }

  public addServiceSection(section: ServiceSection) {
    const hasSection = this.services.find(s => s.id === section.id);
    if (!hasSection) {
      this.services.push(section);
    }
  }

  public clearServiceEndpoint() {
    this.services = [];
  }

  public async digest(): Promise<Buffer> {
    return JsonLdHelpers.digestJsonLd(this.toJSON(), this.context);
  }

  public toJSON(): IDidDocument {
    return classToPlain(this) as IDidDocument;
  }

  public prepareProof(keyId: string, signature: string) {
    this.proof = new Ed25519Signature2018();
    this.proof.creator = keyId;
    this.proof.signature = signature;
    this.proof.nonce = Helpers.generateRandomHex(8);
  }
}
