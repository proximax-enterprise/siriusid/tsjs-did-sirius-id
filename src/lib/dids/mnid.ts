import { Payload, MnidModel, Mnid } from '../..';
import { Convert, RawAddress } from 'tsjs-xpx-chain-sdk';

import { Security } from '../utils/security';

/**
 * Generate checksum from the payload
 * @function generateChecksum
 * @param {Buffer[]} payload - payload.
 * @returns - 4-bytes checksum of the payload.
 */
const generateChecksum = (payload: Payload) => {
  if (payload === undefined) {
    throw new Error('payload not given');
  }
  return Buffer.from(
    Security.sha3_256Hash(Buffer.concat(payload)),
    'hex',
  ).slice(0, 4);
};

/**
 * Convert to multiple network identifier with specific network and address.
 * @function toMNID
 * @param {object} params - Encode parameters.
 * @param {string} params.network - The ProximaX Sirius Chain network identifier.
 * @param {string} params.address - The ProximaX Sirius Chain account address
 * @returns {string} MNID - The ProximaX Sirius encoded mulitiple network identifier
 */
export const toMNID = ({ network, address }: MnidModel): Mnid => {
  const payload = [
    Buffer.from('01', 'hex'), // version
    Buffer.from(Convert.hexToUint8(network.slice(2)).buffer), // network type
    Buffer.from(RawAddress.stringToAddress(address).buffer), // address
  ];
  const checksum = generateChecksum(payload);
  payload.push(checksum);
  return Security.base58Encode(Buffer.concat(payload));
};

/**
 * Retrieve network and address information from MNID.
 * @function fromMNID
 * @param {string} mnid - the ProximaX Sirius encoded multiple network identifier.
 * @returns {{network: string, address: string}} The information of ProximaX Sirius Chain network and account address.
 */
export const fromMNID = (
  mnid: Mnid,
): { readonly network: string; readonly address: string } => {
  const data = Buffer.from(Security.base58Decode(mnid));
  const version = data.slice(0, 1);
  const network = data.slice(1, 2);
  const address = data.slice(2, data.length - 4);
  const check = data.slice(data.length - 4);

  const checksum = generateChecksum([version, network, address]);
  if (!check.equals(checksum)) {
    throw new Error('Invalid address checksum');
  }

  return {
    network: `0x${Convert.uint8ToHex(network)}`.toLowerCase(),
    address: `${RawAddress.addressToString(address)}`.toUpperCase(),
  };
};
