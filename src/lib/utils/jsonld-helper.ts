import { normalize } from 'jsonld';

import { Security } from './security';
import {
  ILinkedDataSignatureAttrs,
  ISignedJsonLdObject,
} from '../dids/document/document-common';
import { JsonLdContext, IJsonLdObject } from '../common/types';
export class JsonLdHelpers {
  public static async normalizeJsonLd(
    { ['@context']: _, ...data }: IJsonLdObject,
    context: JsonLdContext,
  ) {
    return normalize(data, {
      expandContext: context,
    });
  }

  public static async normalizeLdProof(
    { signatureValue, id, type, ...toNormalize }: ILinkedDataSignatureAttrs,
    context: JsonLdContext,
  ): Promise<string> {
    return JsonLdHelpers.normalizeJsonLd(toNormalize, context);
  }

  public static async digestJsonLd(
    { proof, ['@context']: _, ...data }: ISignedJsonLdObject,
    context: JsonLdContext,
  ): Promise<Buffer> {
    return Security.sha256Hash(
      Buffer.concat([
        Security.sha256Hash(
          Buffer.from(await JsonLdHelpers.normalizeLdProof(proof, context)),
        ),
        Security.sha256Hash(
          Buffer.from(await JsonLdHelpers.normalizeJsonLd(data, context)),
        ),
      ]),
    );
  }
}
