import { NetworkIdentifer, Did } from '../common/types';
import { Address } from 'tsjs-xpx-chain-sdk';
import { createDid } from '../dids/did';
import { toNetworkType } from './converter';
import { lib } from 'crypto-js';

export class Helpers {
  public static getDidFromKeyId(keyId: string): Did {
    return keyId.substring(0, keyId.indexOf('#'));
  }

  public static getDidFromPublicKey(
    publicKey: string,
    network: NetworkIdentifer,
  ): Did {
    const networkType = toNetworkType(network);
    const address = Address.createFromPublicKey(publicKey, networkType).plain();
    return createDid({ network, address });
  }

  public static generateRandomHex(nr: number): string {
    return lib.WordArray.random(nr);
  }
}
