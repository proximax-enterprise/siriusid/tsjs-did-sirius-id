import test from 'ava';
import { NetworkType } from 'tsjs-xpx-chain-sdk';
import { toNetworkIdentifier, toNetworkType } from './converter';

test('should convert to network identifier', t => {
  const networkType = NetworkType.PRIVATE_TEST;
  const networkId = toNetworkIdentifier(networkType);
  t.log(networkId);
  t.deepEqual(networkId, '0xb0');
});

test('should convert to network type', t => {
  const networkId = '0xb0';
  const networkType = toNetworkType(networkId);
  t.log(networkType);
  t.deepEqual(networkType, NetworkType.PRIVATE_TEST);
});
