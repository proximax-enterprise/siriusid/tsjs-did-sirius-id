import { NetworkType } from 'tsjs-xpx-chain-sdk';
import { NetworkIdentifer } from '../common/types';

export const toNetworkIdentifier = (networkType: NetworkType) => {
  return `0x${networkType.toString(16)}`;
};

export const toNetworkType = (network: NetworkIdentifer): NetworkType => {
  const networkNum: number = parseInt(network, 16);
  return networkNum;
};
