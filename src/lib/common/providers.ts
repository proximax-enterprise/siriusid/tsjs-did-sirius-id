import { ResourceHash, IKeyPair, NetworkIdentifer } from './types';
import { Stream } from 'stream';
import { SiriusAccount } from '../sirius-account';

export interface IStorageProvider {
  getType(): string;
  save(stream: Stream): Promise<ResourceHash>;
  read(hash: string): Promise<Stream>;
  isOnline(): Promise<boolean>;
}

export interface IKeyVaultProvider {
  generateKeyPair(network: NetworkIdentifer): IKeyPair;
  createFromMasterKey(network: NetworkIdentifer, privateKey: string): IKeyPair;
}

export interface IRegistryProvider {
  publish(keyPair: IKeyPair, resource: ResourceHash): Promise<string>;
  read(address: string): Promise<ResourceHash>;
  isOnline(): Promise<boolean>;
  getNetwork(): Promise<NetworkIdentifer>;
  createAccount(): Promise<SiriusAccount>;
  createAccountFromKey(privateKey: string): Promise<SiriusAccount>;
}
