import { createDid, resolveDid } from './dids/did';
import * as QRCode from 'qrcode';

import {
  UNABLE_TO_CREATE_SIRIUS_ID,
  SIRIUS_ID_NOT_FOUND,
  SIRIUS_ID_DOCUMENT_NOT_FOUND,
  SIRIUS_ID_DOCUMENT_EXISTS,
  REGISTRY_PROVIDER_UNAVAILABLE,
  STORAGE_PROVIDER_UNAVAILABLE,
} from './common/constants';
import { DidDocument } from './dids/document/document';
import { IDidDocument } from './dids/document/document-common';
import { StreamHelper } from './utils/stream-helper';
import { Did, Identity } from './common/types';
import { IStorageProvider, IRegistryProvider } from './common/providers';
import { PublicKeySection } from './dids/document/public-key';
import { AuthenticationSection } from './dids/document/authentication';
import { SiriusAccount } from './sirius-account';
import { toNetworkIdentifier } from './utils/converter';

export class SiriusId {
  public static async createInstance(
    registryProvider: IRegistryProvider,
    storageProvider: IStorageProvider,
  ) {
    const isRegistryOnline = await registryProvider.isOnline();
    const isStorageOnline = await storageProvider.isOnline();

    if (!isRegistryOnline) {
      throw new TypeError(REGISTRY_PROVIDER_UNAVAILABLE);
    }

    if (!isStorageOnline) {
      throw new TypeError(STORAGE_PROVIDER_UNAVAILABLE);
    }

    return new SiriusId(registryProvider, storageProvider);
  }

  private registryProvider: IRegistryProvider;
  private storageProvider: IStorageProvider;

  constructor(
    registryProvider: IRegistryProvider,
    storageProvider: IStorageProvider,
  ) {
    this.registryProvider = registryProvider;
    this.storageProvider = storageProvider;
  }

  public async resolve(did: Did): Promise<DidDocument> {
    try {
      const { address } = resolveDid(did);

      const { hash } = await this.registryProvider.read(address);

      if (!hash) {
        throw new TypeError(SIRIUS_ID_NOT_FOUND);
      }

      const docStream = await this.storageProvider.read(hash);
      const docContent = await StreamHelper.stream2String(docStream);
      const didDocument = DidDocument.fromJSON(
        JSON.parse(docContent) as IDidDocument,
      );

      return didDocument;
    } catch (err) {
      throw new TypeError(`${SIRIUS_ID_DOCUMENT_NOT_FOUND} ${err.message}`);
    }
  }

  public async create(
    privateKey?: string,
    operations?: any,
  ): Promise<Identity> {
    const account: SiriusAccount = privateKey
      ? await this.registryProvider.createAccountFromKey(privateKey)
      : await this.registryProvider.createAccount();

    const did = createDid({
      network: toNetworkIdentifier(account.address.networkType),
      address: account.address.plain(),
    });

    const qrCode = await QRCode.toDataURL(did);

    let reason = '';
    try {
      await this.resolve(did);
      reason = SIRIUS_ID_DOCUMENT_EXISTS;
    } catch (err) {
      // generate basic document base on did
      const didDocument = DidDocument.createFromDid(did);

      // add default public key sections
      const keyId = `${did}#keys-1`;
      if (didDocument.publicKeys.length <= 0) {
        const publicKeySection = PublicKeySection.create(
          account.publicKey,
          keyId,
          did,
        );
        didDocument.addPublicKeySection(publicKeySection);
      }

      // add default authentication section
      if (didDocument.authentications.length <= 0) {
        const authenticationSection = AuthenticationSection.create(
          didDocument.publicKeys[0],
        );
        didDocument.addAuthenticationSection(authenticationSection);
      }

      // perform operations on the document
      if (operations) {
        operations(didDocument);
      }

      // sign the document
      const docContent = JSON.stringify(didDocument.toJSON());
      const signature = account.signData(docContent);
      didDocument.prepareProof(keyId, signature);

      // update document
      const updateContent = JSON.stringify(didDocument.toJSON());

      const docStream = StreamHelper.string2Stream(updateContent);

      const resource = await this.storageProvider.save(docStream);

      const tx = await this.registryProvider.publish(
        { privateKey: account.privateKey, publicKey: account.publicKey },
        resource,
      );

      return { account, did, didDocument, qrCode, referenceTx: tx };
    }

    throw new TypeError(`${UNABLE_TO_CREATE_SIRIUS_ID} "${did}". ${reason}`);
  }

  public async update(privateKey: string, operations?: any): Promise<Identity> {
    const account: SiriusAccount = await this.registryProvider.createAccountFromKey(
      privateKey,
    );

    const did = createDid({
      network: toNetworkIdentifier(account.address.networkType),
      address: account.address.plain(),
    });

    const didDocument = await this.resolve(did);

    if (operations) {
      operations(didDocument);
    }

    // sign the document
    const docContent = JSON.stringify(didDocument.toJSON());
    const signature = account.signData(docContent);

    // create linked data signature proof

    didDocument.signature = signature;
    // didDocument.prepareProof(didDocument.publicKeys[0].id, signature);
    didDocument.updated = new Date();

    // update document
    const updateContent = JSON.stringify(didDocument.toJSON());

    const docStream = StreamHelper.string2Stream(updateContent);

    const resource = await this.storageProvider.save(docStream);

    // remove old content from storage

    const tx = await this.registryProvider.publish(
      { privateKey: account.privateKey, publicKey: account.publicKey },
      resource,
    );

    return { account, did, didDocument, referenceTx: tx };
  }
}
