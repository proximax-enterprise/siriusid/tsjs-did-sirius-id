# SiriusId DID Method Specification

v0.1, Thomas Tran

## Overview

The ProximaX SiriusID DID method specification conforms to the requirements specified in the DID specification currently published by the W3C Credentials Community Group. For more information about DIDs and DID method specifications, please see the [DID Primer](https://github.com/WebOfTrustInfo/rwot5-boston/blob/master/topics-and-advance-readings/did-primer.md and [DID Spec](https://w3c-ccg.github.io/did-spec/). It's core technologies are the [ProximaX Sirius Chain Platform](https://proximax.io) and [ProximaX Storage](https://proximax.io)

ProximaX SiriusID is intended to implement DIDs and DID Documents.

## Target System

The target system is the ProximaX Sirius Chain network. This can either be:

- Sirius Chain on Main Net
- Sirius Chain on Test Net
- Sirius Chain on Private Net

## DID Method Name

The name string that shall identify this DID method is `sirius`

A DID that uses this method MUST begin with the following prefix: `did:sirius`. Per the DID specification, this string MUST be in lowercase. The remainder of the DID, after the prefix, is specified below.

## Method Specific Identifier

The SiriusID DID schema is defined by the following:

```abnf
siriusid-did = "did:sirius:" idstring

idstring = base58(version,network-identifier,address)
```

- version: the version of the did, default is 1.0
- network-identifier: the ProximaX Chain network type, default is Main_Net
- address: the ProximaX Chain unique account address


### Example

A valid SiriusID DID might be:

```abnf
did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj
```

## Operation Definitions

### Create (Register)

The new unique `sirius` DID can be registered via the ProximaX Sirius Chain Registry. A new account in ProximaX Sirius Chain will be created. This account represents the newly created `sirius` DID. The DID document might be stored in the ProximaX Sirius Storage, the hash of the DID document associated with the `sirius` DID and the link to actual `sirius` DID document in the storage attached to the account as metadata. 

The minimal `sirius` DID document for an identifier e.g., `2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj` looks like this

```jsonld
{
  "@context": ['https://w3id.org/did/v1', 'https://w3id.org/security/v2'],
  "id": "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj",
  "authentication": [
    "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj#keys-1"
  ]
  "publicKey": [{
    "id": "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj#signing-key-1",
    "type": "Ed25519VerificationKey2018",
    "controller": "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj",
    "publicKeyHex": "58773C5D5DBE4339FDFD1267F1D5E60127C5BC4760E6E884D9E7ABCD57994748"
  }]
}
```

### Read (Resolve)

A Sirius ID DID document can be retrieved by the given `sirius` DID via the ProximaX Sirius Registry. The ProximaX Sirius Registry parses the `sirius` DID to obtain the ProximaX Sirius Chain network and the account address. From the extracted information, query the ProximaX Sirius Chain to retrieve the `sirius` DID document hash and link from the account metadata. 

The `sirius` DID document retrieved from the document link via the ProximaX Sirius Storage and verify with the document hash.

### Update (Replace)

To update the `sirius` DID document, the owner of the DID should update the new DID document, and re-publish via the ProximaX Sirius Registry. The generated DID document hash and link to the DID Document in the ProximaX Sirius Storage will be updated in the account metadata.

### Delete (Revoke) - N/A

A registered `sirius` DID can be deactivated by simply trigger the revoke function by the given `sirius` DID via the ProximaX Sirius Registry. The ProximaX Sirius Registry parses the `sirius` DID to obtain the ProximaX Sirius Chain network and the account address. From the extracted information, remove the `sirius` DID document hash and link from the account metadata

## Security Considerations

The core consensus algorithm for SiriusID utilizes the Twisted Edwards curve with the digital signature algorithm called Ed25519 cryptographic suite. 

### Key pair
The key pair consists of a private key and a public key:

- Private key: A random 256-bit integer used to sign entities.
- Public key: The public identifier of the key pair. Proves that the entity was signed with the paired private key. The public key is cryptographically derived from the private key. 

### A ProximaX Chain address is a base-32 encoded triplet consisting of:

- The network byte.
- The 160-bit hash of the account’s public key.
- The 4 byte checksum, to allow quick recognition of mistyped addresses.

## Privacy 

No personal information stored in the DID document. DID documents in SiriusID should be limited to just public keys and service endpoints for key recovery.

The public key will be used to encrypt data, only those with the private key can decrypt and access the data.

The private key only exists on the user's device and will not be known to any third party.

## Reference

- [DID Method Registry](https://w3c-ccg.github.io/did-method-registry/)
